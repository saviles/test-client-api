﻿using AspCoreModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoreApiClient
{
    public partial class ApiClient
    {
        public async Task<List<ExpedientModel>> GetExpedient()
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "api/expedient"));
            return await GetAsync<List<ExpedientModel>>(requestUrl);
        }

        public async Task<Message<ExpedientModel>> SaveExpedient(ExpedientModel model)
        {
            var requestUrl = CreateRequestUri(string.Format(System.Globalization.CultureInfo.InvariantCulture,
                "api/expedient"));
            return await PostAsync<ExpedientModel>(requestUrl, model);
        }
    }
}
