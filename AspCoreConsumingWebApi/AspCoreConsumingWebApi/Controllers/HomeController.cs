﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreConsumingWebApi.Factory;
using AspCoreConsumingWebApi.Models;
using AspCoreConsumingWebApi.Utility;
using AspCoreModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

namespace AspCoreConsumingWebApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOptions<MySettingsModel> appSettings;

        public HomeController(IOptions<MySettingsModel> app)
        {
            appSettings = app;
            ApplicationSettings.WebApiUrl = appSettings.Value.WebApiBaseUrl;
        }

        public async Task<IActionResult> Index()
        {
            IEnumerable<ExpedientModel> expedientList = null;
            expedientList = await ApiClientFactory.Instance.GetExpedient();
            return View(expedientList);
        }

        public ActionResult Create()
        {
            CreateExpedientModel createExpedientModel = new CreateExpedientModel();

            return View(createExpedientModel);
        }

        [HttpPost]
        public async Task<JsonResult> Create(ExpedientModel expedientModel)
        {
            var response = await ApiClientFactory.Instance.SaveExpedient(model);
            return Json(response);
        }
    }
}