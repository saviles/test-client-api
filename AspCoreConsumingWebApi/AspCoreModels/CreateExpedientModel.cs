﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspCoreModels
{
    public class CreateExpedientModel
    {
        public string Name { get; set; }
        public int BloodTyId { get; set; }
        public IList<BloodTypeModel> BloodTypes { get; set; } = new List<BloodTypeModel>();
        public int AllergyId { get; set; }
        public IList<AllergyModel> Allergies { get; set; } = new List<AllergyModel>();
    }
}
