﻿namespace AspCoreModels
{
    public class BloodTypeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
