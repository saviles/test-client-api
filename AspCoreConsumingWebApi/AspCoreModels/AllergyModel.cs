﻿namespace AspCoreModels
{
    public class AllergyModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public MedicineModel Medicine { get; set; }
    }
}
