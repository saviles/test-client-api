﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspCoreModels
{
    public class PatientModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public BloodTypeModel BloodType { get; set; }
        public IList<AllergyModel> Allergies { get; set; } = new List<AllergyModel>();
    }
}
