﻿namespace AspCoreModels
{
    public class ExpedientModel
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public PatientModel Patient { get; set; }
    }
}
