# README #

Web application and RESTful API regarding the registration and consultation of patient expedient.

## Frameworks and Libraries
- [ASP.NET Core](https://docs.microsoft.com/pt-br/aspnet/core/?view=aspnetcore-3.1);
- [Entity Framework Core](https://docs.microsoft.com/en-us/ef/core/) (for data access);
- [AutoMapper](https://automapper.org/) (for mapping resources and models);
- [Swashbuckle](https://github.com/domaindrivendev/Swashbuckle) (API documentation).

After cloning the project compile and run the API
In the appsettings of the AspCoreConsumingWebApi project modify the WebApiBaseUrl variable for the API locale
Then compile and run the AspCoreConsumingWebApi project