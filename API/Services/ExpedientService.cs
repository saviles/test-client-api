﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using API.Domain.Models;
using API.Domain.Models.Queries;
using API.Domain.Repositories;
using API.Domain.Services;
using API.Domain.Services.Communication;
using API.Infrastructure;

namespace API.Services
{
    public class ExpedientService : IExpedientService
    {
        private readonly IExpedientRepository _expedientRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMemoryCache _cache;

        public ExpedientService(IExpedientRepository expedientRepository, IUnitOfWork unitOfWork, IMemoryCache cache)
        {
            _expedientRepository = expedientRepository;
            _unitOfWork = unitOfWork;
            _cache = cache;
        }

        public async Task<QueryResult<Expedient>> ListAsync(ExpedientQuery query)
        {
            var expedientList = await _cache.GetOrCreateAsync(CacheKeys.ExpedientList, (entry) => {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(1);
                return _expedientRepository.ListAsync(query);
            });

            return expedientList;
        }

        public async Task<ExpedientResponse> SaveAsync(Expedient expedient)
        {
            try
            {
                await _expedientRepository.AddAsync(expedient);
                await _unitOfWork.CompleteAsync();

                return new ExpedientResponse(expedient);
            }
            catch (Exception ex)
            {
                return new ExpedientResponse($"An error occurred when saving the expedient: {ex.Message}");
            }
        }
    }
}
