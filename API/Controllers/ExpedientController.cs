﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using API.Domain.Models;
using API.Domain.Models.Queries;
using API.Domain.Services;
using API.Resources;

namespace API.Controllers
{
    [Route("api/expedient")]
    [Produces("application/json")]
    [ApiController]
    public class ExpedientController : Controller
    {
        private readonly IExpedientService _expedientService;
        private readonly IMapper _mapper;

        public ExpedientController(IExpedientService expedientService, IMapper mapper)
        {
            _expedientService = expedientService;
            _mapper = mapper;
        }

        /// <summary>
        /// Lists all existing expedient
        /// </summary>
        /// <returns>List of expedient</returns>
        [HttpGet]
        [ProducesResponseType(typeof(QueryResultResource<ExpedientResource>), 200)]
        public async Task<QueryResultResource<ExpedientResource>> ListAsync([FromQuery] ExpedientQueryResource query)
        {
            var expedientQuery = _mapper.Map<ExpedientQueryResource, ExpedientQuery>(query);
            var queryResult = await _expedientService.ListAsync(expedientQuery);

            var resource = _mapper.Map<QueryResult<Expedient>, QueryResultResource<ExpedientResource>>(queryResult);
            return resource;
        }

        /// <summary>
        /// Saves a new expedient
        /// </summary>
        /// <param name="resource">Expedient data</param>
        /// <returns>Response for the request</returns>
        [HttpPost]
        [ProducesResponseType(typeof(ExpedientResource), 201)]
        [ProducesResponseType(typeof(ErrorResource), 400)]
        public async Task<IActionResult> PostAsync([FromBody] SaveExpedientResource resource)
        {
            var expedient = _mapper.Map<SaveExpedientResource, Expedient>(resource);
            var result = await _expedientService.SaveAsync(expedient);

            if (!result.Success)
            {
                return BadRequest(new ErrorResource(result.Message));
            }

            var expedientResource = _mapper.Map<Expedient, ExpedientResource>(result.Resource);
            return Ok(expedientResource);
        }
    }
}
