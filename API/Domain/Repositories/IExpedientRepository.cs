﻿using System.Threading.Tasks;
using API.Domain.Models;
using API.Domain.Models.Queries;

namespace API.Domain.Repositories
{
    public interface IExpedientRepository
    {
        Task<QueryResult<Expedient>> ListAsync(ExpedientQuery query);
        Task AddAsync(Expedient expedient);
    }
}
