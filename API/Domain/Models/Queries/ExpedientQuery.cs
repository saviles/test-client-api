﻿namespace API.Domain.Models.Queries
{
    public class ExpedientQuery : Query
    {
        public string Number { get; set; }

        public ExpedientQuery(string number, int page, int itemsPerPage) : base(page, itemsPerPage)
        {
            Number = number;
        }
    }
}
