﻿using System.Collections.Generic;

namespace API.Domain.Models
{
    public class Patient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ExpedientId { get; set; }
        public Expedient Expedient { get; set; }
        public BloodType BloodType { get; set; }
        public IList<Allergy> Allergies { get; set; } = new List<Allergy>();
    }
}
