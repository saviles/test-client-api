﻿namespace API.Domain.Models
{
    public class BloodType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
