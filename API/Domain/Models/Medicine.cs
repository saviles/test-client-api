﻿namespace API.Domain.Models
{
    public class Medicine
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int AllergyId { get; set; }
        public Allergy Allergy { get; set; }
    }
}
