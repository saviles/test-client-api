﻿namespace API.Domain.Models
{
    public class Expedient
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public Patient Patient { get; set; }
    }
}
