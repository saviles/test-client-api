﻿namespace API.Domain.Models
{
    public class Allergy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Medicine Medicine { get; set; }
    }
}
