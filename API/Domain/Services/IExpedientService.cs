﻿using System.Threading.Tasks;
using API.Domain.Models;
using API.Domain.Models.Queries;
using API.Domain.Services.Communication;

namespace API.Domain.Services
{
    public interface IExpedientService
    {
        Task<QueryResult<Expedient>> ListAsync(ExpedientQuery query);
        Task<ExpedientResponse> SaveAsync(Expedient expedient);
    }
}
