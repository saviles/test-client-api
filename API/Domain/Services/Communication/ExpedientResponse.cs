﻿using API.Domain.Models;

namespace API.Domain.Services.Communication
{
    public class ExpedientResponse : BaseResponse<Expedient>
    {
        public ExpedientResponse(Expedient expedient) : base(expedient) { }

        public ExpedientResponse(string message) : base(message) { }
    }
}
