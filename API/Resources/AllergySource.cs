﻿using System.ComponentModel.DataAnnotations;

namespace API.Resources
{
    public class AllergySource
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public MedicineResource Medicine { get; set; }
    }
}
