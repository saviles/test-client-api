﻿using System.ComponentModel.DataAnnotations;

namespace API.Resources
{
    public class SaveExpedientResource
    {
        [Required]
        public PatientResource Patient { get; set; }
    }
}
