﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.Resources
{
    public class PatientResource
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        public BloodTypeResource BloodType { get; set; }

        [Required]
        public List<AllergySource> Allergies { get; set; }
    }
}
