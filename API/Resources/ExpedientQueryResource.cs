﻿namespace API.Resources
{
    public class ExpedientQueryResource : QueryResource
    {
        public string Number { get; set; }
    }
}
