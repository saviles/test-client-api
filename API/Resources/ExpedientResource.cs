﻿namespace API.Resources
{
    public class ExpedientResource
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public PatientResource Patient { get; set; }
    }
}
