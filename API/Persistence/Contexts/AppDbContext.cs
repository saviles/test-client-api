using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.InMemory.ValueGeneration.Internal;
using API.Domain.Models;
using System.Collections.Generic;

namespace API.Persistence.Contexts
{
    public class AppDbContext : DbContext
    {
        public DbSet<Expedient> Expedient { get; set; }
        public DbSet<Patient> Patient { get; set; }
        public DbSet<BloodType> BloodType { get; set; }
        public DbSet<Allergy> Allergy { get; set; }
        public DbSet<Medicine> Medicine { get; set; }

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Expedient>().ToTable("Expedient");
            builder.Entity<Expedient>().HasKey(p => p.Id);
            builder.Entity<Expedient>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Expedient>().Property(p => p.Number).IsRequired().HasMaxLength(36);
            builder.Entity<Expedient>().HasOne(a => a.Patient).WithOne(b => b.Expedient).HasForeignKey<Patient>(b => b.ExpedientId);

            //builder.Entity<Expedient>().HasData
            //(
            //    new Expedient { Id = 100, Number = System.Guid.NewGuid().ToString(), Patient = new Patient() { Id = 100, Name = "Test patient 1" } },
            //    new Expedient { Id = 101, Number = System.Guid.NewGuid().ToString(), Patient = new Patient() { Id = 101, Name = "Test patient 2" } }
            //);

            builder.Entity<Patient>().ToTable("Patient");
            builder.Entity<Patient>().HasKey(p => p.Id);
            builder.Entity<Patient>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Patient>().Property(p => p.Name).IsRequired().HasMaxLength(50);
            builder.Entity<Patient>().HasOne(a => a.BloodType);
            builder.Entity<Patient>().HasMany(p => p.Allergies);

            //builder.Entity<Patient>().HasData
            //(
            //    new Patient { Id = 102, Name = "Test patient 3", BloodType = new BloodType(){ Id=100, Name= "Test Blood Type 1" }, Allergies = new List<Allergy>() { new Allergy() { Id = 100, Name = "Test Allergy 1", Medicine = new Medicine() { Id = 100, Name = "Test medicine 1" } } } },
            //    new Patient { Id = 103, Name = "Test patient 4", BloodType = new BloodType() { Id = 101, Name = "Test Blood Type 2" }, Allergies = new List<Allergy>() { new Allergy() { Id = 101, Name = "Test Allergy 2", Medicine = new Medicine() { Id = 101, Name = "Test medicine 2" } } } }
            //);

            builder.Entity<BloodType>().ToTable("BloodType");
            builder.Entity<BloodType>().HasKey(p => p.Id);
            builder.Entity<BloodType>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<BloodType>().Property(p => p.Name).IsRequired().HasMaxLength(50);

            //builder.Entity<BloodType>().HasData
            //(
            //    new BloodType { Id = 102, Name = "Test blood type 3" },
            //    new BloodType { Id = 103, Name = "Test blood type 4" }
            //);

            builder.Entity<Allergy>().ToTable("Allergy");
            builder.Entity<Allergy>().HasKey(p => p.Id);
            builder.Entity<Allergy>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Allergy>().Property(p => p.Name).IsRequired().HasMaxLength(50);
            builder.Entity<Allergy>().HasOne(a => a.Medicine).WithOne(p => p.Allergy).HasForeignKey<Medicine>(a => a.AllergyId);

            //builder.Entity<Allergy>().HasData
            //(
            //    new Allergy { Id = 102, Name = "Test allergy 3", Medicine = new Medicine(){ Id = 100, Name = "Test medicine 1" } },
            //    new Allergy { Id = 103, Name = "Test allergy 4", Medicine = new Medicine() { Id = 101, Name = "Test medicine 2" } }
            //);

            builder.Entity<Medicine>().ToTable("Medicine");
            builder.Entity<Medicine>().HasKey(p => p.Id);
            builder.Entity<Medicine>().Property(p => p.Id).IsRequired().ValueGeneratedOnAdd();
            builder.Entity<Medicine>().Property(p => p.Name).IsRequired().HasMaxLength(50);

            //builder.Entity<Medicine>().HasData
            //(
            //    new Medicine { Id = 102, Name = "Test medicine 3" },
            //    new Medicine { Id = 103, Name = "Test medicine 4" }
            //);
        }
    }
}