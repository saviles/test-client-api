﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using API.Domain.Models;
using API.Domain.Models.Queries;
using API.Domain.Repositories;
using API.Persistence.Contexts;

namespace API.Persistence.Repositories
{
    public class ExpedientRepository : BaseRepository, IExpedientRepository
    {
        public ExpedientRepository(AppDbContext context) : base(context) { }

		public async Task<QueryResult<Expedient>> ListAsync(ExpedientQuery query)
		{
			IQueryable<Expedient> queryable = _context.Expedient
													.Include(p => p.Patient)
													.AsNoTracking();
			if (!string.IsNullOrEmpty(query.Number))
			{
				queryable = queryable.Where(p => p.Number.Contains(query.Number));
			}

			int totalItems = await queryable.CountAsync();

			List<Expedient> expedient = await queryable.Skip((query.Page - 1) * query.ItemsPerPage)
													.Take(query.ItemsPerPage)
													.ToListAsync();
			return new QueryResult<Expedient>
			{
				Items = expedient,
				TotalItems = totalItems,
			};
		}

		public async Task AddAsync(Expedient expedient)
		{
			expedient.Number = System.Guid.NewGuid().ToString();
			await _context.Expedient.AddAsync(expedient);
		}
	}
}
